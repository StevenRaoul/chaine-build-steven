package fr.stevia.chainebuild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChaineBuildApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaineBuildApplication.class, args);
	}

}
